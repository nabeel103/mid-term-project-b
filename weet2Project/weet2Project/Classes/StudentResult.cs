﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weet2Project.Classes
{
    class StudentResult
    {
        int studentId;
        int assessmentComponentId;
        int rubricMeasurementId;
        DateTime evaluationDate;

        public int StudentId
        {
            get
            {
                return studentId;
            }

            set
            {
                studentId = value;
            }
        }

        public int AssessmentComponentId
        {
            get
            {
                return assessmentComponentId;
            }

            set
            {
                assessmentComponentId = value;
            }
        }

        public int RubricMeasurementId
        {
            get
            {
                return rubricMeasurementId;
            }

            set
            {
                rubricMeasurementId = value;
            }
        }

        public DateTime EvaluationDate
        {
            get
            {
                return evaluationDate;
            }

            set
            {
                evaluationDate = value;
            }
        }
        public StudentResult()
        {
            this.studentId = -1;
            this.assessmentComponentId = -1;
            this.rubricMeasurementId = -1;
            this.evaluationDate = DateTime.Now;
        }
        public StudentResult(int studentId, int assessmentComponentId, int rubricMeasurementId, DateTime evaluationDate)
        {
            this.studentId = studentId;
            this.assessmentComponentId = assessmentComponentId;
            this.rubricMeasurementId = rubricMeasurementId;
            this.evaluationDate = evaluationDate;
        }
       
        
        
    }
}

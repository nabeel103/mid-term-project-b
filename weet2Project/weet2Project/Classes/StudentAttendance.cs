﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace weet2Project.Classes
{
    class StudentAttendance
    {
        int attendanceId;
        int studentId;
        int attendanceStatus;

        public int AttendanceId
        {
            get
            {
                return attendanceId;
            }

            set
            {
                attendanceId = value;
            }
        }

        public int StudentId
        {
            get
            {
                return studentId;
            }

            set
            {
                studentId = value;
            }
        }

        public int AttendanceStatus
        {
            get
            {
                return attendanceStatus;
            }

            set
            {
                attendanceStatus = value;
            }
        }
        public StudentAttendance()
        {
            this.AttendanceId = -1;
            this.StudentId = -1;
            this.AttendanceStatus = -1;
        }
        public StudentAttendance(int attendanceId, int studentId, int attendanceStatus)
        {
            this.AttendanceId = attendanceId;
            this.StudentId = studentId;
            this.AttendanceStatus = attendanceStatus;
        }
        public static int addUpdateStudentAttendance(StudentAttendance studentAttendance)
        {
            bool flag = true;
            int count = 0;
            //MessageBox.Show(studentAttendance.attendanceId.ToString());

            String std = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(std);
            con.Open();

            StudentAttendance _studentAttendance = new StudentAttendance();

            if (con.State == System.Data.ConnectionState.Open)
            {

                SqlCommand cmd = new SqlCommand("SELECT * FROM StudentAttendance WHERE AttendanceId = '" + studentAttendance.attendanceId + "' AND StudentId = '" + studentAttendance.studentId + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    _studentAttendance.attendanceId = read.GetInt32(0);
                    _studentAttendance.studentId = read.GetInt32(1);
                    _studentAttendance.attendanceStatus = read.GetInt32(2);

                    if (((_studentAttendance.attendanceId != -1) && (_studentAttendance.studentId != -1) &&
                        (_studentAttendance.attendanceStatus != -1)) && ((_studentAttendance.attendanceStatus != studentAttendance.attendanceStatus)
                    )
                   )
                    {
                        Configuration.getInstance().Execute("UPDATE StudentAttendance SET AttendanceStatus = '" + studentAttendance.attendanceStatus + "' WHERE StudentId = '" + studentAttendance.studentId + "' AND AttendanceId = '" + studentAttendance.attendanceId + "'");
                        count = 2;
                        flag = false;

                    }
                    break;
                }
                read.Close();
            }
            if (flag)
            {
                if (con.State == System.Data.ConnectionState.Open)
                {
                    SqlCommand cmd = new SqlCommand("SELECT * FROM StudentAttendance WHERE AttendanceId = '" + studentAttendance.attendanceId + "' AND  StudentId = '" + studentAttendance.studentId + "'", con);
                    read = cmd.ExecuteReader();
                    while (!(read.Read()))
                    {
                        //MessageBox.Show(studentAttendance.attendanceId.ToString() +" "+ studentAttendance.StudentId.ToString()+" " + studentAttendance.attendanceStatus.ToString());

                        Configuration.getInstance().Execute("INSERT INTO StudentAttendance(AttendanceId, StudentId, AttendanceStatus) values ('" + studentAttendance.attendanceId + "', '" + studentAttendance.StudentId + "', '" + studentAttendance.attendanceStatus + "')");
                        count = 1;
                        break;
                    }
                    read.Close();
                }

            }

            return count;
        }
        public static bool deleteStudentAttendance(int attendanceId, int studentId)
        {
            int exe = Configuration.getInstance().Execute("DELETE FROM StudentAttendance WHERE StudentId = '" + studentId + "' AND AttendanceId = '" + attendanceId + "'");
            if (exe > 0)
                return true;
            else
                return false;
        }
        
        public static List<StudentAttendance> getStudentAttendanceList()
        {
            string std = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(std);
            con.Open();

            List<StudentAttendance> list = new List<StudentAttendance>();
            

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM StudentAttendance ", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    StudentAttendance attendance = new StudentAttendance();
                    attendance.attendanceId = read.GetInt32(0);
                    attendance.studentId = read.GetInt32(1);
                    attendance.attendanceStatus = read.GetInt32(2);

                    list.Add(attendance);
                }
            }
            con.Close();
            return list;
        }
        public static List<StudentAttendance> getAttendanceList_SID(int studentId)
        {
            string std = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(std);
            con.Open();

            List<StudentAttendance> list = new List<StudentAttendance>();
            if (con.State == System.Data.ConnectionState.Open)
            {
               
                SqlCommand cmd = new SqlCommand("SELECT * FROM StudentAttendance WHERE StudentId = '" + studentId + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    StudentAttendance attendance = new StudentAttendance();

                    attendance.attendanceId = read.GetInt32(0);
                    attendance.studentId = read.GetInt32(1);
                    attendance.attendanceStatus = read.GetInt32(2);

                    list.Add(attendance);
                }
            }
            con.Close();
            return list;
        }
        public static List<StudentAttendance> getStudentAttendanceList(int attendanceId)
        {
            string std = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(std);
            con.Open();

            List<StudentAttendance> list = new List<StudentAttendance>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM StudentAttendance WHERE AttendanceId = '" + attendanceId + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    StudentAttendance attendance = new StudentAttendance();

                    attendance.attendanceId = read.GetInt32(0);
                    attendance.studentId = read.GetInt32(1);
                    attendance.attendanceStatus = read.GetInt32(2);

                    list.Add(attendance);
                }
            }
            con.Close();
            return list;
        }

    }
}

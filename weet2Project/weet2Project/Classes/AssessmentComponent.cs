﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weet2Project.Classes
{
    public class AssessmentComponent
    {
        int id;
        string name;
        int rubricId;
        int totalMarks;
        DateTime dateCreated;
        DateTime dateUpdated;
        int assessmentId;
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public int RubricId
        {
            get
            {
                return rubricId;
            }

            set
            {
                rubricId = value;
                dateUpdated = DateTime.Now;
            }
        }
        public int TotalMarks
        {
            get
            {
                return totalMarks;
            }

            set
            {
                totalMarks = value;
                dateUpdated = DateTime.Now;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                dateUpdated = DateTime.Now;
            }
        }
        public DateTime DateCreated
        {
            get
            {
                return dateCreated;
            }
            set
            {
                dateCreated = value;
            }
        }
        public DateTime DateUpdated
        {
            get
            {
                return dateUpdated;
            }
            set
            {
                dateUpdated = value;
            }
        }
        public int AssessmentId
        {
            get
            {
                return assessmentId;
            }

            set
            {
                assessmentId = value;
                dateUpdated = DateTime.Now;
            }
        }
        //Default constructor 
        public AssessmentComponent()
        {
            this.id = -1;
            this.name = "";
            this.rubricId = -1;
            this.totalMarks = -1;
            this.dateCreated = DateTime.Now;
            this.dateUpdated = DateTime.Now;
            this.assessmentId = -1;
        }
        //Parameterized constructor 
        public AssessmentComponent(string name, int rubricId, int totalMarks, int assessmentId)
        {
            this.id = -1;
            this.name = name;
            this.rubricId = rubricId;
            this.totalMarks = totalMarks;
            this.dateCreated = DateTime.Now;
            this.dateUpdated = DateTime.Now;
            this.assessmentId = assessmentId;
        }
        public static AssessmentComponent getAssessmentComponent(string name, int assessmentId)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            AssessmentComponent component = new AssessmentComponent();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("Select * From AssessmentComponent Where Name = '" + name + "' AND AssessmentId = '" + assessmentId + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    component.id = read.GetInt32(0);
                    component.name = read.GetString(1);
                    component.rubricId = read.GetInt32(2);
                    component.totalMarks = read.GetInt32(3);
                    component.dateCreated = read.GetDateTime(4);
                    component.dateUpdated = read.GetDateTime(5);
                    component.assessmentId = read.GetInt32(6);
                    break;
                }
            }

            con.Close();
            return component;
        }

        public static AssessmentComponent getAssessmentComponent_Id(int id)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            AssessmentComponent component = new AssessmentComponent();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM AssessmentComponent WHERE Id = '" + id + "'", con);

                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    component.id = read.GetInt32(0);
                    component.name = read.GetString(1);
                    component.rubricId = read.GetInt32(2);
                    component.totalMarks = read.GetInt32(3);
                    component.dateCreated = read.GetDateTime(4);
                    component.dateUpdated = read.GetDateTime(5);
                    component.assessmentId = read.GetInt32(6);
                    break;
                }
            }
            con.Close();
            return component;
        }

        public static int addUpdateAssessmentComponent(AssessmentComponent component)
        {
            int check = 0;
            bool flag = true;

            if (component.name == "")
                return check;

            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            AssessmentComponent assessmentComponent = new AssessmentComponent();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM AssessmentComponent WHERE Id = '" + component.id + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    assessmentComponent.id = read.GetInt32(0);
                    assessmentComponent.name = read.GetString(1);
                    assessmentComponent.rubricId = read.GetInt32(2);
                    assessmentComponent.totalMarks = read.GetInt32(3);
                    assessmentComponent.dateCreated = read.GetDateTime(4);
                    assessmentComponent.dateUpdated = read.GetDateTime(5);
                    assessmentComponent.assessmentId = read.GetInt32(6);

                    flag = false;
                    break;
                }
                read.Close();
            }
            if (flag)
            {
                if (con.State == System.Data.ConnectionState.Open)
                {
                    SqlCommand cmd = new SqlCommand("SELECT * FROM AssessmentComponent WHERE Name = '" + component.name + "'AND RubricId = '" + component.rubricId + "'AND AssessmentId = '" + component.assessmentId + "'", con);
                    read = cmd.ExecuteReader();

                    while (!(read.Read()))
                    {
                        Configuration.getInstance().Execute("INSERT INTO AssessmentComponent(Name, RubricId, TotalMarks, DateCreated, DateUpdated, AssessmentId) VALUES ('" + component.name + "', '" + component.rubricId + "', '" + component.totalMarks + "', '" + component.DateCreated + "', '" + component.dateUpdated + "', '" + component.assessmentId + "')");
                        check = 1;
                        break;
                    }
                    read.Close();
                }
                
            }
            else
            {
                AssessmentComponent a = AssessmentComponent.getAssessmentComponent(component.name, component.assessmentId);
                int id = a.id;

                if ((!assessmentComponent.name.Equals(""))
                    && ((assessmentComponent.name != component.name) ||
                      (assessmentComponent.rubricId != component.rubricId) ||
                      (assessmentComponent.totalMarks != component.totalMarks) ||
                      (assessmentComponent.assessmentId != component.assessmentId))
                      && (id == -1 || id == assessmentComponent.id))
                {
                    Configuration.getInstance().Execute("UPDATE AssessmentComponent SET Name = '" + component.name + "', RubricId = '" + component.rubricId + "', TotalMarks = '" + component.totalMarks + "', AssessmentId = '" + component.assessmentId + "' WHERE Id = '" + component.id + "'");
                    check = 2;
                }
            }
            con.Close();
            return check;
        }
        public static bool deleteAssessmentComponent_Id(int id)
        {
            String deleteQuery = "DELETE FROM AssessmentComponent WHERE Id = '" + id + "'";
            int exe = Configuration.getInstance().Execute(deleteQuery);

            if (exe > 0)
            {

                return true;
            }
            else
                return false;
        }
        
        public static List<AssessmentComponent> getAssessmentComponentList(int assessmentId)
        {
            String str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            List<AssessmentComponent> list = new List<AssessmentComponent>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM AssessmentComponent WHERE AssessmentId = '" + assessmentId + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    AssessmentComponent component = new AssessmentComponent();

                    component.id = read.GetInt32(0);
                    component.name = read.GetString(1);
                    component.rubricId = read.GetInt32(2);
                    component.totalMarks = read.GetInt32(3);
                    component.dateCreated = read.GetDateTime(4);
                    component.dateUpdated = read.GetDateTime(5);
                    component.assessmentId = read.GetInt32(6);

                    list.Add(component);
                }
            }
            con.Close();

            return list;
        }

        
        
    }
}

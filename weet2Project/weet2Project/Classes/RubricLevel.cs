﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weet2Project.Classes
{
    public class RubricLevel
    {
        int id;
        int rubricId;
        string details;
        int measurementLevel;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public int RubricId
        {
            get
            {
                return rubricId;
            }
            set
            {
                rubricId = value;
            }
        }

        public string Details
        {
            get
            {
                return details;
            }

            set
            {
                details = value;
            }
        }

        public int MeasurementLevel
        {
            get
            {
                return measurementLevel;
            }

            set
            {
                measurementLevel = value;
            }
        }
        public RubricLevel()
        {
            this.id = -1;
            this.rubricId = -1;
            this.details = "";
            this.measurementLevel = -1;
        }
        public RubricLevel(int rubricId, string details, int measurementLevel)
        {
            this.id = -1;
            this.rubricId = rubricId;
            this.details = details;
            this.measurementLevel = measurementLevel;
        }
        public static int addUpdateRubricLevel(RubricLevel rubricLevel)
        {
            bool flag = true;
            int count = 0;

            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            RubricLevel _rubricLevel = new RubricLevel();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE Id = '" + rubricLevel.id + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    _rubricLevel.id = read.GetInt32(0);
                    _rubricLevel.rubricId = read.GetInt32(1);
                    _rubricLevel.details = read.GetString(2);
                    _rubricLevel.measurementLevel = read.GetInt32(3);

                    int id = RubricLevel.getRubricLevel(rubricLevel.rubricId, rubricLevel.details, rubricLevel.measurementLevel).id;
                    if ((rubricLevel.rubricId != -1 && rubricLevel.details != "") && (_rubricLevel.details != "") && ((_rubricLevel.details != rubricLevel.details) || (_rubricLevel.rubricId != rubricLevel.rubricId) || (_rubricLevel.measurementLevel != rubricLevel.measurementLevel)) && (id == -1 || id == _rubricLevel.id))
                    {
                        Configuration.getInstance().Execute("UPDATE RubricLevel SET RubricId = '" + rubricLevel.rubricId + "', Details = '" + rubricLevel.details + "', MeasurementLevel = '" + rubricLevel.measurementLevel + "' WHERE Id = '" + rubricLevel.id + "'");
                        count = 2;
                        flag = false;
                    }
                    break;
                }
                read.Close();
            }
            if (flag)
            {
                if (con.State == System.Data.ConnectionState.Open)
                {
                    SqlDataReader r1 = null;
                    SqlDataReader r2 = null;

                    SqlCommand cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE RubricId = '" + rubricLevel.rubricId + "'AND Details = '" + rubricLevel.details + "'", con);

                    r1 = cmd.ExecuteReader();
                    cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE RubricId = '" + rubricLevel.rubricId + "' AND MeasurementLevel = '" + rubricLevel.measurementLevel + "'", con);
                    bool r1Flag = true;
                    while(r1.Read())
                    {
                        r1Flag = false;
                        break;
                    }
                    r1.Close();
                    //r2.Close();
                    r2 = cmd.ExecuteReader();

                    while (!(r1Flag && r2.Read()))
                    {
                        Configuration.getInstance().Execute("INSERT INTO RubricLevel(RubricId, Details, MeasurementLevel) VALUES ('" + rubricLevel.rubricId + "', '" + rubricLevel.details + "', '" + rubricLevel.measurementLevel + "')");
                        count = 1;
                        break;
                    }
                    r1.Close();
                    r2.Close();
                }
            }
            con.Close();

            return count;
        }
        public static bool deleteRubricLevels(int id)
        {
            
            int exe = Configuration.getInstance().Execute("DELETE FROM RubricLevel WHERE Id = '" + id + "'");
            if (exe > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static RubricLevel getRubricLevel(int rubricId, string details, int measurementLevel)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            RubricLevel rubricLevel = new RubricLevel();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE RubricId = '" + rubricId + "' AND Details = '" + details + "' AND MeasurementLevel = '" + measurementLevel + "'", con);

                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    rubricLevel.id = read.GetInt32(0);
                    rubricLevel.rubricId = read.GetInt32(1);
                    rubricLevel.details = read.GetString(2);
                    rubricLevel.measurementLevel = read.GetInt32(3);
                    break;
                }
            }
            con.Close();
            return rubricLevel;
        }
       
        public static RubricLevel getRubricLevel(int id)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            RubricLevel rubricLevel = new RubricLevel();

            if (con.State == System.Data.ConnectionState.Open)
            {

                SqlCommand cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE Id = '" + id + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    rubricLevel.id = read.GetInt32(0);
                    rubricLevel.rubricId = read.GetInt32(1);
                    rubricLevel.details = read.GetString(2);
                    rubricLevel.measurementLevel = read.GetInt32(3);
                    break;
                }
            }
            con.Close();
            return rubricLevel;
        }

        
        
        
    }
}

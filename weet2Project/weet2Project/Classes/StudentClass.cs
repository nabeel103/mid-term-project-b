﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace weet2Project.Classes
{
    class StudentClass
    {
        int id;
        string firstName;
        string lastName;
        string contact;
        string email;
        string registrationNumber;
        int status;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string FirstName
        {
            get
            {
                return firstName;
            }

            set
            {
                firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }

            set
            {
                lastName = value;
            }
        }

        public string Contact
        {
            get
            {
                return contact;
            }

            set
            {
                contact = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string RegistrationNumber
        {
            get
            {
                return registrationNumber;
            }

            set
            {
                registrationNumber = value;
            }
        }

        public int Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public static object MesssageBox { get; private set; }

        //default Constructor 
        public StudentClass()
        {
            this.id = -1;
            this.firstName = "";
            this.lastName = "";
            this.contact = "";
            this.email = "";
            this.registrationNumber = "";
            this.status = -1;
        }
        public StudentClass(string firstName, string lastName, string contact, string email, string registrationNumber, int status)
        {
            this.id = -1;
            this.firstName = firstName;
            this.lastName = lastName;
            this.contact = contact;
            this.email = email;
            this.registrationNumber = registrationNumber;
            this.status = status;
        }
        public static int Add_and_UpdateStudent(StudentClass student)
        {
            int count = 0;
            bool flag = true;

            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            StudentClass std = new StudentClass();

            if (con.State == System.Data.ConnectionState.Open)
            {

                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Id = '" + student.id + "'", con);
                data = cmd.ExecuteReader();

                while (data.Read())
                {
                    std.id = data.GetInt32(0);
                    std.firstName = data.GetString(1);
                    std.lastName = data.GetString(2);
                    std.contact = data.GetString(3);
                    std.email = data.GetString(4);
                    std.registrationNumber = data.GetString(5);
                    std.status = data.GetInt32(6);

                    if ((std.firstName != "" && std.lastName != "" && std.contact != "" && std.email != "" && std.registrationNumber != "" && std.status != -1) && (std.firstName != student.firstName || std.lastName != student.lastName || std.contact != student.contact || std.email != student.email || std.registrationNumber != student.registrationNumber || std.status != student.status) && ((StudentClass.getStudent(student.contact, student.email, student.registrationNumber).id == std.id) || (StudentClass.getStudent_contact(student.contact).id == -1 || StudentClass.getStudent_email(student.email).id == -1 || StudentClass.getStudent__RegistrationNumber(student.registrationNumber).id == -1)))

                    {
                        Configuration.getInstance().Execute("UPDATE Student SET FirstName = '" + student.firstName + "', LastName = '" + student.lastName + "', Contact = '" + student.contact + "', Email = '" + student.email + "', RegistrationNumber = '" + student.registrationNumber + "', Status = '" + student.status + "' WHERE Id = '" + student.id + "'");
                        count = 2;
                        flag = false;
                    }
                    break;
                }
                data.Close();
            }
            if (flag)
            {

                if (con.State == System.Data.ConnectionState.Open)
                {


                    SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Contact = '" + student.contact + "' OR Email = '" + student.email + "' OR RegistrationNumber = '" + student.registrationNumber + "'", con);
                    data = cmd.ExecuteReader();

                    while (!(data.Read()))
                    {
                        Configuration.getInstance().Execute("INSERT INTO Student(FirstName, LastName, Contact, Email, RegistrationNumber, Status) values ('" + student.firstName + "', '" + student.lastName + "', '" + student.contact + "', '" + student.email + "', '" + student.registrationNumber + "', '" + student.status + "')");
                        count = 1;
                        break;
                    }
                    data.Close();
                }

            }

            con.Close();
            return count;
        }

        public static bool deleteStudent(int id)
        {
            List<StudentAttendance> sAttList = StudentAttendance.getAttendanceList_SID(id);
            foreach (StudentAttendance att in sAttList)
            {
                StudentAttendance.deleteStudentAttendance(att.AttendanceId, att.StudentId);
            }

           
            int exe = Configuration.getInstance().Execute("DELETE FROM Student WHERE Id = '" + id + "'");
            if (exe > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public static StudentClass getStudent(string contact, string email, string registrationNumber)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            StudentClass std = new StudentClass();

            if (con.State == System.Data.ConnectionState.Open)
            {
               
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Contact = '" + contact + "' AND Email = '" + email + "' AND RegistrationNumber = '" + registrationNumber + "'", con);
                data = cmd.ExecuteReader();

                while (data.Read())
                {
                    std.id = data.GetInt32(0);
                    std.firstName = data.GetString(1);
                    std.lastName = data.GetString(2);
                    std.contact = data.GetString(3);
                    std.email = data.GetString(4);
                    std.registrationNumber = data.GetString(5);
                    std.status = data.GetInt32(6);

                    break;
                }
            }
            con.Close();

            return std;
        }
        public static StudentClass getStudent(int id)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            StudentClass std = new StudentClass();

            if (con.State == System.Data.ConnectionState.Open)
            {

                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Id = '" + id + "'", con);
                data = cmd.ExecuteReader();

                while (data.Read())
                {
                    std.id = data.GetInt32(0);
                    std.firstName = data.GetString(1);
                    std.lastName = data.GetString(2);
                    std.contact = data.GetString(3);
                    std.email = data.GetString(4);
                    std.registrationNumber = data.GetString(5);
                    std.status = data.GetInt32(6);

                    break;
                }
            }
            con.Close();

            return std;
        }
        public static StudentClass getStudent_contact(string contact)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            StudentClass student = new StudentClass();

            if (con.State == System.Data.ConnectionState.Open)
            {
              
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Contact = '" + contact + "'", con);
                data = cmd.ExecuteReader();

                while (data.Read())
                {
                    student.id = data.GetInt32(0);
                    student.firstName = data.GetString(1);
                    student.lastName = data.GetString(2);
                    student.contact = data.GetString(3);
                    student.email = data.GetString(4);
                    student.registrationNumber = data.GetString(5);
                    student.status = data.GetInt32(6);

                    break;
                }
            }
            con.Close();
            return student;
        }
        public static StudentClass getStudent_email(string email)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            StudentClass std = new StudentClass();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Email = '" + email + "'", con);
                data = cmd.ExecuteReader();

                while (data.Read())
                {
                    std.id = data.GetInt32(0);
                    std.firstName = data.GetString(1);
                    std.lastName = data.GetString(2);
                    std.contact = data.GetString(3);
                    std.email = data.GetString(4);
                    std.registrationNumber = data.GetString(5);
                    std.status = data.GetInt32(6);

                    break;
                }
            }
            con.Close();

            return std;
        }

        public static StudentClass getStudent__RegistrationNumber(string registrationNumber)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            StudentClass student = new StudentClass();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE RegistrationNumber = '" + registrationNumber + "'", con);
                data = cmd.ExecuteReader();

                while (data.Read())
                {
                    student.id = data.GetInt32(0);
                    
                    student.firstName = data.GetString(1);
                    student.lastName = data.GetString(2);
                    student.contact = data.GetString(3);
                    student.email = data.GetString(4);
                    student.registrationNumber = data.GetString(5);
                    student.status = data.GetInt32(6);

                    break;
                }
            }
            con.Close();
           
            return student;
        }
        
        public static List<StudentClass> getStudentList()
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            List<StudentClass> list = new List<StudentClass>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student", con);
                data = cmd.ExecuteReader();
                while (data.Read())
                {
                    StudentClass std = new StudentClass();

                    std.id = data.GetInt32(0);
                    std.firstName = data.GetString(1);
                    std.lastName = data.GetString(2);
                    std.contact = data.GetString(3);
                    std.email = data.GetString(4);
                    std.registrationNumber = data.GetString(5);
                    std.status = data.GetInt32(6);
                    

                    list.Add(std);
                }
            }
            con.Close();

            return list;
        }
        public static List<StudentClass> getActiveStudentList()
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            List<StudentClass> list = new List<StudentClass>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Status = 5", con);
                data = cmd.ExecuteReader();
                while (data.Read())
                {
                    StudentClass std = new StudentClass();

                    std.id = data.GetInt32(0);
                    std.firstName = data.GetString(1);
                    std.lastName = data.GetString(2);
                    std.contact = data.GetString(3);
                    std.email = data.GetString(4);
                    std.registrationNumber = data.GetString(5);
                    std.status = data.GetInt32(6);

                    list.Add(std);
                }
            }
            con.Close();
            return list;
        }
        public static List<StudentClass> getStudentsList_Status(int status)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader data = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            List<StudentClass> list = new List<StudentClass>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Status = '" + status + "'", con);
                data = cmd.ExecuteReader();
                while (data.Read())
                {
                    StudentClass std = new StudentClass();

                    std.id = data.GetInt32(0);
                    std.firstName = data.GetString(1);
                    std.lastName = data.GetString(2);
                    std.contact = data.GetString(3);
                    std.email = data.GetString(4);
                    std.registrationNumber = data.GetString(5);
                    std.status = data.GetInt32(6);

                    list.Add(std);
                }
            }
            con.Close();
            return list;
        }

        

    }
}

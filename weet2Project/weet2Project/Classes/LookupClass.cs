﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weet2Project.Classes
{
    public class Lookup
    {
        int id;
        string name;
        string category;

        public int LookupId
        {
            private set
            {
                id = value;
            }
            get
            {
                return id;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Category
        {
            get
            {
                return category;
            }

            set
            {
                category = value;
            }
        }

        public Lookup()
        {
            this.id = -1;
            this.name = "";
            this.category = "";
        }
       

        public static Lookup getLookup(string name, string category)
        {
            
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            Lookup lookup = new Lookup();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Lookup WHERE Name = '" + name + "' AND Category = '" + category + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    lookup.id = read.GetInt32(0);
                    lookup.name = read.GetString(1);
                    lookup.category = read.GetString(2);
                    break;
                }
            }
            con.Close();
            return lookup;
        }

        public static Lookup getLookup(int lookupId)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            Lookup lookup = new Lookup();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Lookup WHERE LookupId = '" + lookupId.ToString() + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    lookup.id = read.GetInt32(0);
                    lookup.name = read.GetString(1);
                    lookup.category = read.GetString(2);
                    break;
                }
            }
            con.Close();
            return lookup;
        }

        public static List<Lookup> getLookupList()
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            List<Lookup> list = new List<Lookup>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Lookup", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    Lookup lookup = new Lookup();

                    lookup.id = read.GetInt32(0);
                    lookup.name = read.GetString(1);
                    lookup.category = read.GetString(2);

                    list.Add(lookup);
                }
            }
            con.Close();
            return list;
        }

        
        public static List<Lookup> getLookupList_category(string category)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            List<Lookup> list = new List<Lookup>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Lookup WHERE Category = '" + category + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    Lookup lookup = new Lookup();

                    lookup.id = read.GetInt32(0);
                    lookup.name = read.GetString(1);
                    lookup.category = read.GetString(2);
                    list.Add(lookup);
                }
            }
            con.Close();
            return list;
        }

        
    }
}

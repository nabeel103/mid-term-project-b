﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weet2Project.Classes
{
    public class Rubric
    {
        int id;
        string details;
        int cloId;
        


        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Details
        {
            get
            {
                return details;
            }

            set
            {
                details = value;
            }
        }

        public int CloId
        {
            get
            {
                return cloId;
            }
            set
            {
                cloId = value;
            }
        }
       
        
        public Rubric()
        {
            this.id = -1;
            this.details = "";
            this.cloId = -1;
        }
        public Rubric(string details, int cloId)
        {
            this.id = -1;
            this.details = details;
            this.cloId = cloId;
        }
        public static int addRubric(Rubric rubric)
        {
            int count = 0;
            bool flag = true;

            String str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            Rubric _rubric = new Rubric();

            if (con.State == System.Data.ConnectionState.Open)
            {
                string q = "SELECT * FROM Rubric WHERE Id = '" + rubric.id + "'";
                SqlCommand cmd = new SqlCommand(q, con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    _rubric.id = read.GetInt32(0);
                    _rubric.details = read.GetString(1);
                    _rubric.cloId = read.GetInt32(2);

                    int x = Rubric.getRubric(rubric.details, rubric.cloId).id;

                    if ((rubric.cloId != -1 && rubric.details != "") && (_rubric.details != "") && ((_rubric.details != rubric.details) || (_rubric.cloId != rubric.cloId)) && (x == -1 || x == _rubric.id))
                    {
                        Configuration.getInstance().Execute("UPDATE Rubric SET Details = '" + rubric.details + "', CloId = '" + rubric.cloId + "' WHERE Id = '" + rubric.id + "'");
                        count = 2;
                    }
                    flag = false;
                    break;
                }
                read.Close();
            }
            if (flag)
            {

                if (con.State == System.Data.ConnectionState.Open)
                {

                    SqlCommand cmd = new SqlCommand("SELECT * FROM Rubric WHERE Details = '" + rubric.details + "' AND CloId = '" + rubric.cloId + "'", con);
                    read = cmd.ExecuteReader();
                    int newId = 0;

                    while (!(read.Read()))
                    {

                        //SqlDataReader read_2 = null;
                        if (con.State == System.Data.ConnectionState.Open)
                        {

                            cmd = new SqlCommand("SELECT MAX(Id) FROM Rubric", con);
                            read.Close();
                            read = cmd.ExecuteReader();
                            while (read.Read() && !read.IsDBNull(0))
                            {

                                newId = read.GetInt32(0);
                                newId++;
                                break;
                            }
                        }

                        Configuration.getInstance().Execute("INSERT INTO Rubric(Id, Details, CloId) VALUES ('" + newId + "', '" + rubric.details + "', '" + rubric.cloId + "')");
                        count = 1;
                        break;
                    }
                    read.Close();
                }

            }

            con.Close();
            return count;
        }

        public static bool deleteRubric(int id)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            List<AssessmentComponent> componentList = new List<AssessmentComponent>();
            List<RubricLevel> rubricLevelList = new List<RubricLevel>();

            //delete all rubric levels associated with this rubtic 
            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM RubricLevel WHERE RubricId = '" + id + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    RubricLevel rubricLevel = new RubricLevel();
                    rubricLevel.Id = read.GetInt32(0);
                    rubricLevel.RubricId = read.GetInt32(1);
                    rubricLevel.Details = read.GetString(2);
                    rubricLevel.MeasurementLevel = read.GetInt32(3);

                    rubricLevelList.Add(rubricLevel);
                }
            }

            
            foreach (RubricLevel rl in rubricLevelList)
            {
                RubricLevel.deleteRubricLevels(rl.Id);
            }


            //delete all assessment components associated with this rubtic 


            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM AssessmentComponent WHERE RubricId = '" + id + "'", con);

                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    AssessmentComponent compontns = new AssessmentComponent();
                    compontns.Id = read.GetInt32(0);
                    compontns.Name = read.GetString(1);
                    compontns.RubricId = read.GetInt32(2);
                    compontns.TotalMarks = read.GetInt32(3);
                    compontns.DateCreated = read.GetDateTime(4);
                    compontns.DateUpdated = read.GetDateTime(5);
                    compontns.AssessmentId = read.GetInt32(6);
                    componentList.Add(compontns);
                }
            }
            foreach (AssessmentComponent c in componentList)
            {
                AssessmentComponent.deleteAssessmentComponent_Id(c.Id);
            }

            //delete rubric
            int exe = Configuration.getInstance().Execute("DELETE FROM Rubric WHERE Id = '" + id + "'");
            con.Close();
            if (exe > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
       
        public static Rubric getRubric(int id)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            Rubric rubric = new Rubric();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Rubric WHERE Id = '" + id + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    rubric.id = read.GetInt32(0);
                    rubric.details = read.GetString(1);
                    rubric.cloId = read.GetInt32(2);
                    break;
                }
            }
            con.Close();
            return rubric;
        }

        public static Rubric getRubric(string details, int cloId)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            Rubric rubric = new Rubric();

            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand("SELECT * FROM Rubric WHERE Details = '" + details + "' AND CloId = '" + cloId + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    rubric.id = read.GetInt32(0);
                    rubric.details = read.GetString(1);
                    rubric.cloId = read.GetInt32(2);
                    break;
                }
            }
            con.Close();
            return rubric;
        }
        
        
        public static List<Rubric> getRubricList()
        {
            String str = Configuration.getInstance().ConnectionStr;
            SqlConnection con = new SqlConnection(str);
            SqlDataReader read = null;
            con.Open();

            List<Rubric> list = new List<Rubric>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Rubric", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    Rubric rubric = new Rubric(read.GetString(1), read.GetInt32(2));
                    rubric.id = read.GetInt32(0);
                    list.Add(rubric);
                }
            }
            con.Close();
            return list;
        }
    }
}

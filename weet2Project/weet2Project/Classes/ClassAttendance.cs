﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace weet2Project.Classes
{
    public class ClassAttendance
    {
        int id;
        DateTime attendanceDate;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public DateTime AttendanceDate
        {
            get
            {
                return attendanceDate;
            }

            set
            {
                attendanceDate = value;
            }
        }
        
        public ClassAttendance()
        {
            this.id = -1;
            this.attendanceDate = DateTime.Now;
        }
        
        public static int addUpdateClassAttendance(ClassAttendance attendance)
        {


            SqlDataReader read = null;
            String str = Configuration.getInstance().ConnectionStr;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            ClassAttendance classAtt = new ClassAttendance();
            bool flag = true;
            int count = 0;

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM ClassAttendance WHERE Id = '" + attendance.id + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    classAtt.id = read.GetInt32(0);
                    classAtt.attendanceDate = read.GetDateTime(1);

                    flag = false;
                    break;
                }
                read.Close();
            }
            if (flag)
            {
                if (con.State == System.Data.ConnectionState.Open)
                {
                    SqlCommand cmd = new SqlCommand("SELECT * FROM ClassAttendance WHERE DAY(AttendanceDate) = '" + attendance.attendanceDate.Day + "'AND MONTH(AttendanceDate) = '" + attendance.attendanceDate.Month + "'AND YEAR(AttendanceDate) = '" + attendance.attendanceDate.Year + "'", con);
                    read = cmd.ExecuteReader();

                    while (!(read.Read()))
                    {
                        Configuration.getInstance().Execute("INSERT INTO ClassAttendance(AttendanceDate) values ('" + attendance.attendanceDate + "')");
                        count = 1;
                        break;
                    }
                    read.Close();
                }
                
            }
            else
            {
                if (classAtt.attendanceDate != attendance.attendanceDate)
                {
                    Configuration.getInstance().Execute("UPDATE ClassAttendance SET AttendanceDate = '" + attendance.attendanceDate + "' WHERE Id = '" + attendance.id + "'");
                    count = 2;
                }
            }
            con.Close();
            return count;
        }
        public static bool deleteClassAttendance(int id)
        {
            int read;

            List<StudentAttendance> list = StudentAttendance.getStudentAttendanceList(id);
            foreach (StudentAttendance s in list)
            {
                StudentAttendance.deleteStudentAttendance(s.AttendanceId, s.StudentId);
            }
            read = Configuration.getInstance().Execute("DELETE FROM ClassAttendance WHERE Id = '" + id + "'");
            if (read > 0)
            {
                return true;
            }
            else
                return false;
        }

        public static List<ClassAttendance> getClassAttendance()
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            List<ClassAttendance> list = new List<ClassAttendance>();
            ClassAttendance classAtt;

            if (con.State == System.Data.ConnectionState.Open)
            {

                SqlCommand cmd = new SqlCommand("SELECT * FROM ClassAttendance", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    classAtt = new ClassAttendance();

                    classAtt.id = read.GetInt32(0);
                    classAtt.attendanceDate = read.GetDateTime(1);

                    list.Add(classAtt);
                }
            }
            con.Close();
            return list;
        }
        public static ClassAttendance getClassAttendance(int id)
        {
            String str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            ClassAttendance attendance = new ClassAttendance();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM ClassAttendance WHERE Id = '" + id.ToString() + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    attendance.id = read.GetInt32(0);
                    attendance.attendanceDate = read.GetDateTime(1);
                    break;
                }
            }
            con.Close();
            return attendance;
        }

        public static ClassAttendance getClassAttendance(DateTime date)
        {
            string str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            ClassAttendance attendance = new ClassAttendance();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM ClassAttendance WHERE YEAR(AttendanceDate) = '" + date.Year + "' AND MONTH(AttendanceDate) = '" + date.Month + "' AND DAY(AttendanceDate) = '" + date.Day + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    attendance.id = read.GetInt32(0);
                    attendance.attendanceDate = read.GetDateTime(1);
                    break;
                }
                    //MessageBox.Show(attendance.id.ToString());
            }
            con.Close();
            return attendance;
        }

        

    }
}

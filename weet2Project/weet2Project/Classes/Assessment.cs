﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weet2Project.Classes
{

    public class Assessment
    {
        int id;
        String title;
        DateTime dateCreated;
        int totalMarks;
        int totalWeightage;

        public string Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
            }
        }
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public int TotalMarks
        {
            get
            {
                return totalMarks;
            }

            set
            {
                totalMarks = value;
            }
        }
        public DateTime DateCreated
        {
            get
            {
                return dateCreated;
            }
            set
            {
                dateCreated = value;
            }
        }
        public int TotalWeightage
        {
            get
            {
                return totalWeightage;
            }

            set
            {
                totalWeightage = value;
            }
        }
        //default constructor 
        public Assessment()
        {
            this.id = -1;
            this.totalWeightage = -1;
            this.title = "";
            this.dateCreated = DateTime.Now;
        }
        //parameterised constructor 
        public Assessment(string t, int m, int w)
        {
            this.id = -1;
            this.title = t;
            this.dateCreated = DateTime.Now;
            this.totalMarks = m;
            this.totalWeightage = w;
        }
        public static Assessment getAssessment(string assmenTtitle)
        {
            String str = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(str);
            con.Open();

            Assessment assessment = new Assessment("", -1, -1);

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Assessment WHERE Title = '" + assmenTtitle + "'", con);

                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    assessment.id = read.GetInt32(0);
                    assessment.title = read.GetString(1);
                    assessment.dateCreated = read.GetDateTime(2);
                    assessment.totalMarks = read.GetInt32(3);
                    assessment.totalWeightage = read.GetInt32(4);
                    break;
                }
            }
            con.Close();
            return assessment;
        }
        
        public static Assessment getAssessment_Id(int id)
        {
            String srt = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(srt);
            con.Open();

            Assessment assessment = new Assessment();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Assessment WHERE Id = '" + id + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    assessment.id = read.GetInt32(0);
                    assessment.title = read.GetString(1);
                    assessment.dateCreated = read.GetDateTime(2);
                    assessment.totalMarks = read.GetInt32(3);
                    assessment.totalWeightage = read.GetInt32(4);
                    break;
                }
            }

            con.Close();
            return assessment;
        }

        public static List<Assessment> getAssessmentList()
        {
            string srt = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(srt);
            con.Open();

            List<Assessment> asstList = new List<Assessment>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Assessment", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    Assessment a = new Assessment();
                    a.id = read.GetInt32(0);
                    a.title = read.GetString(1);
                    a.dateCreated = read.GetDateTime(2);
                    a.totalMarks = read.GetInt32(3);
                    a.totalWeightage = read.GetInt32(4);

                    asstList.Add(a);
                }
            }
            con.Close();
            return asstList;
        }
        public static bool deleteAssessment_Id(int id)
        {
            bool flag = false;
            List<AssessmentComponent> CompList = AssessmentComponent.getAssessmentComponentList(id);
            foreach (AssessmentComponent ac in CompList)
            {
                AssessmentComponent.deleteAssessmentComponent_Id(ac.Id);
            }
            int DeleteExecutionCheck = Configuration.getInstance().Execute("DELETE FROM Assessment WHERE Id = '" + id + "'");
            if (DeleteExecutionCheck > 0)
            {
                flag = true;
                return flag;
            }
            else
            {
                flag = false;
                return flag;
            }
        }

        public static int addORupdateAssessment(Assessment assessment)
        {
            bool flag = true;
            bool flag2 = true;
            int count = 0;

            string srt = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(srt);
            con.Open();

            Assessment ast = new Assessment();
            int asstId = Assessment.getAssessment(assessment.title).id;

            if (con.State == System.Data.ConnectionState.Open)
            {

                SqlCommand cmd = new SqlCommand("SELECT * FROM Assessment WHERE Id = '" + assessment.id + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    ast.id = read.GetInt32(0);
                    ast.title = read.GetString(1);
                    ast.dateCreated = read.GetDateTime(2);
                    ast.totalMarks = read.GetInt32(3);
                    ast.totalWeightage = read.GetInt32(4);

                    flag = false;
                    break;
                }
                read.Close();
            }
            if (flag)
            {
                if (con.State == System.Data.ConnectionState.Open)
                {

                    SqlCommand cmd = new SqlCommand("SELECT * FROM Assessment WHERE Title = '" + assessment.title + "'", con);
                    read = cmd.ExecuteReader();
                    while (read.Read())
                    {
                        flag2 = false;
                        break;
                    }
                    read.Close();
                }
                if (flag2)
                {
                    Configuration.getInstance().Execute("INSERT INTO Assessment(Title, DateCreated, TotalMarks, TotalWeightage) values ('" + assessment.title + "', '" + assessment.dateCreated + "', '" + assessment.totalMarks + "', '" + assessment.totalWeightage + "')");
                    count = 1;
                }

            }
            else
            {

                if ((!ast.title.Equals(""))
                     &&
                     ((ast.title != assessment.title) || (ast.totalMarks != assessment.totalMarks) || (ast.totalWeightage != assessment.totalWeightage))
                     &&
                     (asstId == -1 || asstId == ast.id))

                {
                    Configuration.getInstance().Execute("UPDATE Assessment SET Title = '" + assessment.title + "', TotalMarks = '" + assessment.totalMarks + "', TotalWeightage = '" + assessment.totalWeightage + "' WHERE Id = '" + assessment.id + "'");

                    count = 2;
                }
            }
            con.Close();
            return count;
        }
    }
}

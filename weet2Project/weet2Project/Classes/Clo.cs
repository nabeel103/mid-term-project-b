﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weet2Project.Classes
{
    class Clo
    {
        int id;
        string name;
        DateTime dateCreated;
        DateTime dateUpdated;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                dateUpdated = DateTime.Now;
            }
        }

        public DateTime DateUpdated
        {
            get
            {
                return dateUpdated;
            }
            set
            {
                dateUpdated = value;
            }
        }

        public DateTime DateCreated
        {
            get
            {
                return dateCreated;
            }
            set
            {
                dateCreated = value;
            }
        }
        public Clo()
        {
            this.id = -1;
            this.name = "";
            this.dateCreated = DateTime.Now;
            this.dateUpdated = DateTime.Now;
        }
        public Clo(string name)
        {
            this.id = -1;
            this.name = name;
            this.dateCreated = DateTime.Now;
            this.dateUpdated = DateTime.Now;
        }
        public static int addUpdateClo(Clo clo)
        {
            bool flag = true;
            int count = 0;
            SqlDataReader read = null;
            String srt = Configuration.getInstance().ConnectionStr;
            SqlConnection con = new SqlConnection(srt);
            con.Open();

            Clo _clo = new Clo();

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Clo WHERE Id = '" + clo.id + "'", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    _clo.id = read.GetInt32(0);
                    _clo.name = read.GetString(1);
                    _clo.dateCreated = read.GetDateTime(2);
                    _clo.dateUpdated = read.GetDateTime(3);

                    int cId = Clo.getClo(clo.name).id;

                    if (_clo.name != clo.name && _clo.name != "" && (cId == -1 || cId == _clo.id))
                    {
                        Configuration.getInstance().Execute("UPDATE Clo SET Name = '" + clo.name + "', DateUpdated = '" + clo.dateUpdated.ToString(@"yyyy-MM-dd") + "' WHERE Id = '" + clo.id + "'");
                        count = 2;
                    }
                    flag = false;
                    break;
                }
                read.Close();
            }
            if (flag)
            {

                if (con.State == System.Data.ConnectionState.Open)
                {
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Clo WHERE Name = '" + clo.name + "'", con);
                    read = cmd.ExecuteReader();
                    while (!(read.Read()))
                    {

                        Configuration.getInstance().Execute("INSERT INTO Clo(Name, DateCreated, DateUpdated) values ('" + clo.name + "', '" + clo.dateCreated.ToString(@"yyyy-MM-dd") + "', '" + clo.dateUpdated.ToString(@"yyyy-MM-dd") + "')");
                        count = 1;
                        break;
                    }
                    read.Close();
                }

            }

            con.Close();
            return count;
        }
        public static bool deleteClo(int id)
        {
            String srt = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(srt);
            con.Open();

            List<Rubric> rubrics = new List<Rubric>();

            if (con.State == System.Data.ConnectionState.Open)
            {
                string q = "SELECT * FROM Rubric WHERE CloId = '" + id + "'";
                SqlCommand cmd = new SqlCommand(q, con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    Rubric rubric = new Rubric("Empty", -1);
                    rubric.Id = read.GetInt32(0);
                    rubric.Details = read.GetString(1);
                    rubric.CloId = read.GetInt32(2);

                    rubrics.Add(rubric);
                }
            }
            con.Close();

            foreach (Rubric rb in rubrics)
            {
                Rubric.deleteRubric(rb.Id);
            }
            int exe = Configuration.getInstance().Execute("DELETE FROM Clo WHERE Id = '" + id + "'");
            if (exe > 0)
            {
                return true;
            }
            else return false;
        }

        public static Clo getClo(string name)
        { 
            string srt = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(srt);
            con.Open();

            Clo clo = new Clo("");

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Clo WHERE Name = '" + name + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    clo.id = read.GetInt32(0);
                    clo.name = read.GetString(1);
                    clo.dateCreated = read.GetDateTime(2);
                    clo.dateUpdated = read.GetDateTime(3);
                    break;
                }
            }

            con.Close();
            return clo;
        }

        public static Clo getClo(int ID)
        {
            string srt = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(srt);
            con.Open();

            Clo clo = new Clo("");

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Clo WHERE Id = '" + ID.ToString() + "'", con);
                read = cmd.ExecuteReader();

                while (read.Read())
                {
                    clo.id = read.GetInt32(0);
                    clo.name = read.GetString(1);
                    clo.dateCreated = read.GetDateTime(2);
                    clo.dateUpdated = read.GetDateTime(3);
                    break;
                }
            }
            con.Close();
            return clo;
        }

        public static List<Clo> getCloList()
        {
            string srt = Configuration.getInstance().ConnectionStr;
            SqlDataReader read = null;
            SqlConnection con = new SqlConnection(srt);
            con.Open();

            List<Clo> list = new List<Clo>(); 

            if (con.State == System.Data.ConnectionState.Open)
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM Clo", con);
                read = cmd.ExecuteReader();
                while (read.Read())
                {
                    Clo clo = new Clo();

                    clo.id = read.GetInt32(0);
                    clo.name = read.GetString(1);
                    clo.dateCreated = read.GetDateTime(2);
                    clo.dateUpdated = read.GetDateTime(3);

                    list.Add(clo);
                }
            }
            con.Close();
            return list;
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class CLOform : Form
    {
        private Clo clo = new Clo();
        public CLOform()
        {
            InitializeComponent();
            showCLOtable();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private bool emptyField()
        {
            cloName.Text = "";
            return true;
        }
        private void showCLOtable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            cloTable.DataSource = dt;
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cloName.Text))
            {
                MessageBox.Show("Please Write CLO Name...!");
                return;
            }
            else
            {
                clo.Name = cloName.Text;

                if (Clo.addUpdateClo(clo) == 1)
                    MessageBox.Show("CLO has been ADDED Successfully!");
                else
                    MessageBox.Show("Error: Add Failed");

                clo.Id = -1;
                showCLOtable();
                emptyField();
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(cloName.Text))
            {
                MessageBox.Show("Select CLO!");
                return;
            }
            if (clo.Id == -1) MessageBox.Show("Selection Error");
            else
            {
                clo.Name = cloName.Text;
                clo.DateUpdated = DateTime.Now;

                int status = Clo.addUpdateClo(clo);
                if (status == 0)
                    MessageBox.Show("Error in Updating");
                if (status == 1)
                    MessageBox.Show("Added Successfully!");
                if (status == 2)
                    MessageBox.Show("Updated Successfully!");

                showCLOtable();
                emptyField();
                clo.Id = -1;
            }
        }

        private void cloTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    //Console.WriteLine(e.RowIndex);
                    DataGridViewRow row = this.cloTable.Rows[e.RowIndex];

                    cloName.Text = row.Cells["Name"].Value.ToString();
                    //loaded data of CLO according to specific ID 
                    clo.Id = Convert.ToInt32(row.Cells["Id"].Value.ToString());
                    Clo c = Clo.getClo(clo.Id);
                    clo.DateCreated = clo.DateCreated;
                    clo.DateUpdated = clo.DateUpdated;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error in Selecting Data");
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (clo.Id == -1)
            {
                MessageBox.Show("Select CLO!");
                return;
            }
            if (string.IsNullOrWhiteSpace(cloName.Text))
            {
                MessageBox.Show("Selection Error");
                return;
            }
            if (Clo.deleteClo(clo.Id))
            {
                MessageBox.Show("Deleted Successfully");
            }
            else 
                MessageBox.Show("Error in Deleting!");

            showCLOtable();
            emptyField();
            clo.Id = -1;
        }
    }
}

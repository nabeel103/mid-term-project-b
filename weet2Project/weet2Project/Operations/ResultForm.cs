﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class ResultForm : Form
    {
        public ResultForm()
        {
            InitializeComponent();
            initialLoadings();
        }

        private void addButton_Click(object sender, EventArgs e)
        {

        }

        private void initialLoadings()
        {
            showTable();
            List<Assessment> assessmentList = Assessment.getAssessmentList();
            foreach (Assessment ast in assessmentList)
            {
                assCombo.Items.Add(ast.Title);
            }

            if (assCombo.Items.Count > 0) 
                assCombo.SelectedItem = assCombo.Items[0];
            List<StudentClass> studentList = StudentClass.getStudentList();
            foreach (StudentClass std in studentList)
            {
                redNoCombo.Items.Add(std.RegistrationNumber);
            }
            if (redNoCombo.Items.Count > 0) 
                redNoCombo.SelectedItem = redNoCombo.Items[0];
        }
        private void showTable()
        {
//            select*
//from StudentResult sr, Student s, AssessmentComponent ac, Assessment a, RubricLevel rl, Rubric r
//where sr.StudentId = s.Id AND sr.AssessmentComponentId = ac.Id AND sr.RubricMeasurementId = rl.Id AND rl.RubricId = r.Id AND ac.AssessmentId = a.Id

            //var con = Configuration.getInstance().getConnection();
            //SqlCommand cmd = new SqlCommand("Select * from StudentResult", con);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataTable dt = new DataTable();
            //da.Fill(dt);
            //resultTable.DataSource = dt;
            


        }

        private void attendanceTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                   
                    DataGridViewRow row = this.resultTable.Rows[e.RowIndex];
                    redNoCombo.Text = row.Cells["StudentId"].Value.ToString();
                    assCombo.Text = row.Cells["Assessment"].Value.ToString();
                   
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error in Selecting Data");
            }
        }

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {

        }
    }
}

﻿
namespace weet2Project.Operations
{
    partial class AttendenceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.regLable = new System.Windows.Forms.Label();
            this.attStatusCombo = new System.Windows.Forms.ComboBox();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2Elipse2 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.SuspendLayout();
            // 
            // regLable
            // 
            this.regLable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.regLable.AutoSize = true;
            this.regLable.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regLable.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.regLable.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.regLable.Location = new System.Drawing.Point(17, 13);
            this.regLable.Name = "regLable";
            this.regLable.Size = new System.Drawing.Size(70, 17);
            this.regLable.TabIndex = 13;
            this.regLable.Text = "Course ID";
            this.regLable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // attStatusCombo
            // 
            this.attStatusCombo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.attStatusCombo.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attStatusCombo.ForeColor = System.Drawing.Color.White;
            this.attStatusCombo.FormattingEnabled = true;
            this.attStatusCombo.Location = new System.Drawing.Point(112, 9);
            this.attStatusCombo.Name = "attStatusCombo";
            this.attStatusCombo.Size = new System.Drawing.Size(160, 25);
            this.attStatusCombo.TabIndex = 21;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this.attStatusCombo;
            // 
            // guna2Elipse2
            // 
            this.guna2Elipse2.BorderRadius = 40;
            this.guna2Elipse2.TargetControl = this;
            // 
            // AttendenceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.Controls.Add(this.attStatusCombo);
            this.Controls.Add(this.regLable);
            this.Name = "AttendenceControl";
            this.Size = new System.Drawing.Size(291, 43);
            this.Load += new System.EventHandler(this.AttendenceControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label regLable;
        private System.Windows.Forms.ComboBox attStatusCombo;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse2;
    }
}

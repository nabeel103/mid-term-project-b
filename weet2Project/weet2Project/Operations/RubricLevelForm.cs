﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class RubricLevelForm : Form
    {
        private static RubricLevel rubricLevel = new RubricLevel();
        public RubricLevelForm()
        {

            InitializeComponent();
            showRubricLevelTable();
            InitialLoadings();
        }
        private void InitialLoadings()
        {
            rubIdCombo.Items.Clear();
            List<Rubric> rubricList = Rubric.getRubricList();
            foreach (Rubric rb in rubricList)
            {
                rubIdCombo.Items.Add(rb.Id);
            }

            if (rubIdCombo.Items.Count > 0)
            {
                rubIdCombo.SelectedItem = rubIdCombo.Items[0];
            }
            showRubricLevelTable();

        }
        private void showRubricLevelTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            rubricLevelTable.DataSource = dt;
        }
        private bool checkFields()
        {
            if(!(string.IsNullOrWhiteSpace(rubricLevelDetails.Text) || string.IsNullOrWhiteSpace(measureLevel.Text) ||string.IsNullOrWhiteSpace(rubIdCombo.Text) ||string.IsNullOrWhiteSpace(RubricDetails.Text)))
            {
                return false;
            }
            return true;
        }
        private void emptyFields()
        {
            rubricLevelDetails.Text = "";
            measureLevel.Text = "";
            RubricDetails.Text = "";
            rubIdCombo.Text = "";
           
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            if (!checkFields())
            {
                rubricLevel.Id = -1;
                rubricLevel.Details = rubricLevelDetails.Text;
                rubricLevel.MeasurementLevel = Int32.Parse(measureLevel.Text);
                rubricLevel.RubricId = Int32.Parse(rubIdCombo.Text);

                if (rubricLevel.RubricId == -1) 
                    MessageBox.Show("Select a valid CLO!");
                else
                {
                    if (RubricLevel.addUpdateRubricLevel(rubricLevel) == 1) 
                        MessageBox.Show("Added Successfully");
                    else 
                        MessageBox.Show("Adding Failed");
                }
                rubricLevel.Id = -1;
                emptyFields();
                InitialLoadings();
               
            }
            else
            {
                MessageBox.Show("Error");
                return;
            }
        }

        private void rubIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rubIdCombo.Text.Length > 0)
            {
                RubricDetails.Text = Rubric.getRubric(Int32.Parse(rubIdCombo.Text)).Details;
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (!checkFields())
            {
                if (rubricLevel.Id == -1) MessageBox.Show("Select data from table!");
                else
                {
                    if (rubricLevel.RubricId == -1) MessageBox.Show("Select a valid Rubric!");


                    rubricLevel.Details = rubricLevelDetails.Text;
                    rubricLevel.MeasurementLevel = Int32.Parse(measureLevel.Text);
                    rubricLevel.RubricId = Int32.Parse(rubIdCombo.Text);

                    int count = RubricLevel.addUpdateRubricLevel(rubricLevel);
                    if (count == 0) 
                        MessageBox.Show("Error");
                    else if (count == 1) 
                        MessageBox.Show("Added Successfully!");
                    else if (count == 2) 
                        MessageBox.Show("Updated Successfully!");

                    rubricLevel.Id = -1;
                    emptyFields();
                    InitialLoadings();
                }
                
            }
            else
            {
                MessageBox.Show("Error in fields");
                return;
            }
           
        }

        private void rubricLevelTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                   
                    DataGridViewRow row = rubricLevelTable.Rows[e.RowIndex];


                    rubricLevelDetails.Text = row.Cells["Details"].Value.ToString();
                    measureLevel.Text = row.Cells["MeasurementLevel"].Value.ToString();
                    rubIdCombo.Text = row.Cells["RubricId"].Value.ToString();
                    if (rubIdCombo.Text.Length > 0)
                    {
                        RubricDetails.Text = Rubric.getRubric(Int32.Parse(rubIdCombo.Text)).Details;
                    }


                    rubricLevel.Id = Convert.ToInt32(row.Cells["Id"].Value.ToString());
                    rubricLevel.RubricId = Convert.ToInt32(rubIdCombo.SelectedItem);
                    rubricLevel.Details = rubricLevelDetails.Text;
                    rubricLevel.MeasurementLevel = Convert.ToInt32(measureLevel.Text);

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error in Selecting Data");
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (rubricLevel.Id != -1)
            {
                if (!checkFields())
                {
                    if (RubricLevel.deleteRubricLevels(rubricLevel.Id))
                        MessageBox.Show("Deleted Successfully");
                    else 
                        MessageBox.Show("Delete Failed");
                    rubricLevel.Id = -1;
                    emptyFields();
                    InitialLoadings();
                   
                }
                else
                {
                    MessageBox.Show("Check Input Fields");
                    return;
                }
                
                
            }
            else
            {
                MessageBox.Show("Select data from table");
                return;
            }
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

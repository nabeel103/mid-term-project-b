﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class RubricsForm : Form
    {
        private static Rubric rubric = new Rubric();
        public RubricsForm()
        {
            InitializeComponent();
            InitialLoadings();

        }

        private void InitialLoadings()
        {
            cloCombo.Items.Clear();
            List<Clo> cloList = Clo.getCloList();
            foreach (Clo cl in cloList)
            {
                cloCombo.Items.Add(cl.Name);
            }
            if (cloCombo.Items.Count > 0)
            {
                cloCombo.SelectedItem = cloCombo.Items[0];
            }
            showRubricsTable();
        }

        private void showRubricsTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            rubricTable.DataSource = dt;
        }

        private void regNo_TextChanged(object sender, EventArgs e)
        {

        }
        private bool checkFields()
        {
            if (!(string.IsNullOrWhiteSpace(details.Text) || string.IsNullOrWhiteSpace(cloCombo.Text)))
            {
                return false;
            }
            return true;
        }
        public void emptyFields()
        {
            details.Text = "";

            if (cloCombo.Items.Count > 0)
            {
                cloCombo.SelectedItem = cloCombo.Items[0];
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            if (!checkFields())
            {
                rubric.Id = -1;
                rubric.Details = details.Text;
                rubric.CloId = Clo.getClo(cloCombo.Text).Id;

                if (rubric.CloId == -1) 
                    MessageBox.Show("Select a valid CLO!");
                else
                {
                    if (Rubric.addRubric(rubric) == 1)
                        MessageBox.Show("Added Successfully");
                    else 
                        MessageBox.Show("Adding Failed");
                }
                rubric.Id = -1;
                emptyFields();
                showRubricsTable();
               
            }
            else
            {
                MessageBox.Show("Warning: Check Input Fields");
                return;
            }
            

        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (!checkFields())
            {
                if (rubric.Id == -1) 
                    MessageBox.Show("Select data from table");
                else
                {
                    if (rubric.CloId == -1) 
                        MessageBox.Show("Warning: Select a valid CLO!");

                    rubric.Details = details.Text;
                    rubric.CloId = Clo.getClo(cloCombo.Text).Id;

                    int count = Rubric.addRubric(rubric);
                    if (count == 0) 
                        MessageBox.Show("Error in updating");
                    else if (count == 1) 
                        MessageBox.Show("Added Successfully!");
                    else if (count == 2) 
                        MessageBox.Show("Updated Successfully!");

                    rubric.Id = -1;
                    emptyFields();
                    showRubricsTable();
                }
                
            }
            else
            {
                MessageBox.Show("Warning: Check Input Fields");
                return;
            }
            
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (rubric.Id == -1)
            {
                if (!checkFields())
                {
                    if (Rubric.deleteRubric(rubric.Id)) 
                        MessageBox.Show("Deleted Successfully");
                    else 
                        MessageBox.Show("Error in deleting");
                    rubric.Id = -1;
                    emptyFields();
                    showRubricsTable();
                    
                }
                else
                {
                    MessageBox.Show("Select data from table");
                    return;
                }
                
                
            }
            else
            {
                MessageBox.Show("Select data from table");
                return;
            }
            
        }

        private void rubricTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    //Console.WriteLine(e.RowIndex);
                    DataGridViewRow row = this.rubricTable.Rows[e.RowIndex];

                    rubric.Id = Convert.ToInt32(row.Cells["Id"].Value.ToString());
                    rubric.Details = row.Cells["Details"].Value.ToString();
                    rubric.CloId = Convert.ToInt32(row.Cells["Cloid"].Value.ToString());

                    
                    details.Text = rubric.Details;
                    cloCombo.Text = Clo.getClo(rubric.CloId).Name;

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error in Selecting Data");
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

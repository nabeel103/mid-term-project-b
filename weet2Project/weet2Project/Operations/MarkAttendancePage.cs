﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class MarkAttendancePage : Form
    {
        DateTime attDate;
        bool Delete;
        
        public MarkAttendancePage(DateTime date, string regno, Lookup lookup, bool delete)
        {
            InitializeComponent();
            Delete = delete;
            if (regno == "" )
            {
                refresh(date);

            }
            else
            {
                attFlowLayout.Controls.Clear();
                ClassAttendance ca = ClassAttendance.getClassAttendance(date);
                AttendenceControl ctrl = new AttendenceControl(regno, ca);
                ctrl.attendanceStatusCombo(lookup.Name);

                attFlowLayout.Controls.Add(ctrl);
            }
            if (delete)
            {
                confirm.Text = "Delete";
                //confirm.Image.Dispose();
            }
            attDate = date;
            //MessageBox.Show(date.ToString());
        }
        public void refresh(DateTime date)
        {

            attFlowLayout.Controls.Clear();
            List<StudentClass> studentList = StudentClass.getActiveStudentList();
            foreach (StudentClass st in studentList)
            {

                AttendenceControl ctrl = new AttendenceControl(st.RegistrationNumber, ClassAttendance.getClassAttendance(date));
                attFlowLayout.Controls.Add(ctrl);

            }
        }

        private void attFlowLayout_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MarkAttendancePage_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {

            this.Dispose();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (!Delete)
            {
                bool add = false;
                bool update = false;
                foreach (AttendenceControl control in attFlowLayout.Controls)
                {

                    
                    int temp = control.SaveAttendance(ClassAttendance.getClassAttendance(attDate));

                    if (temp == 1) 
                        add = true;
                    if (temp == 2) 
                        update = true;
                }

                if (add  || update ) 
                    MessageBox.Show("RESULT ADDED OR UPDATED");
               
                else 
                    MessageBox.Show("NOTHING UPDATED");
               
            }
            else if (Delete)
            {

                bool delete = false;
                foreach (AttendenceControl ctrl in attFlowLayout.Controls)
                {
                    

                    if (StudentAttendance.deleteStudentAttendance(ClassAttendance.getClassAttendance(attDate).Id, StudentClass.getStudent__RegistrationNumber(ctrl.getReg()).Id))
                    {
                        delete = true;
                    }
                }
                if (delete)
                {
                    MessageBox.Show("Deleted Successfully...");

                }
            }
            this.Dispose();
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

        }
    }
}

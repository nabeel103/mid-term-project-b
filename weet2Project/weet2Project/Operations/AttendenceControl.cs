﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class AttendenceControl : UserControl
    {
        private ClassAttendance currentClassAttendance = null;
        public AttendenceControl(String reg, ClassAttendance classAttendance)
        {
            InitializeComponent();

            currentClassAttendance = classAttendance;

            regLable.Text = reg;
            List<Lookup> lookupList = Lookup.getLookupList();
            foreach (Lookup lu in lookupList)
            {
                if (lu.Category.Equals("ATTENDANCE_STATUS"))
                {
                    attStatusCombo.Items.Add(lu.Name);
                }
            }
            attStatusCombo.SelectedItem = attStatusCombo.Items[0];

        }

        private void AttendenceControl_Load(object sender, EventArgs e)
        {

        }

        public int SaveAttendance(ClassAttendance classAttendance)
        {
            StudentAttendance att = new StudentAttendance();
            att.StudentId = StudentClass.getStudent__RegistrationNumber(regLable.Text).Id;
            att.AttendanceId = classAttendance.Id;
            att.AttendanceStatus = Lookup.getLookup(attStatusCombo.Text, "ATTENDANCE_STATUS").LookupId;

            //MessageBox.Show(att.AttendanceId.ToString());
            int count = StudentAttendance.addUpdateStudentAttendance(att);

            return count;
        }
       
        public void attendanceStatusCombo(string lookup)
        {
            attStatusCombo.Text = lookup;
        }
        public string getReg()
        {
            return regLable.Text;
        }
    }
}

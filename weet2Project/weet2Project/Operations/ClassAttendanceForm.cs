﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class ClassAttendanceForm : Form
    {
        private static ClassAttendance classAtt = new ClassAttendance();
        public ClassAttendanceForm()
        {
            InitializeComponent();
            initialLoadings();
        }
        private void initialLoadings()
        {
            showClassAttendanceTable();
        }
        private void showClassAttendanceTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            classAttnebdanceTable.DataSource = dt;
        }

        private bool checkFields()
        {
            if (!(string.IsNullOrWhiteSpace(datePick.Text))) 
                return false;
            else 
                return true;
        }
        private void setDefaultDate()
        {
            datePick.Value = DateTime.Now;
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            if (!checkFields())
            {
                classAtt.AttendanceDate = datePick.Value;

                if (ClassAttendance.addUpdateClassAttendance(classAtt) == 1) 
                    MessageBox.Show("Added Successfully");
                else 
                    MessageBox.Show("error in adding");

                classAtt.Id = -1;
                setDefaultDate();
                showClassAttendanceTable();
                
            }
            else
            {
                MessageBox.Show("Input Error");
                return;
            }

            
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (!checkFields())
            {
                if (classAtt.Id == -1) 
                    MessageBox.Show("Select from Table");
                else
                {
                    classAtt.AttendanceDate = datePick.Value;
                    int count = ClassAttendance.addUpdateClassAttendance(classAtt);

                    if (count == 0)
                        MessageBox.Show("Error: Update Unsuccessful!");
                    else if (count == 1)
                        MessageBox.Show("Added Successfully!");
                    else if (count == 2)
                        MessageBox.Show("Updated Successfully!");

                    classAtt.Id = -1;
                    setDefaultDate();
                    showClassAttendanceTable();
                }
                
            }
            else
            {
                MessageBox.Show("Input Error");
                return;
            }
            
        }

        private void classAttnebdanceTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    //Console.WriteLine(e.RowIndex);
                    DataGridViewRow row = this.classAttnebdanceTable.Rows[e.RowIndex];

                    //Get ID from Table 
                    classAtt.Id = Convert.ToInt32(row.Cells["Id"].Value.ToString());
                    //Load in object
                    ClassAttendance ca = ClassAttendance.getClassAttendance(classAtt.Id);
                    classAtt.AttendanceDate = ca.AttendanceDate;
                    //Load in fields 
                    datePick.Value = classAtt.AttendanceDate;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error in Selecting Data");
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (classAtt.Id == -1)
            {
                if (!checkFields())
                {
                    if (ClassAttendance.deleteClassAttendance(classAtt.Id)) 
                        MessageBox.Show("Deleted Successfully");
                    else 
                        MessageBox.Show("Deleting Failed");

                    classAtt.Id = -1;
                    setDefaultDate();
                    showClassAttendanceTable();
                   
                }
                else
                {
                    MessageBox.Show("Input Error");
                    return;
                }
                
               
            }
            else
            {
                MessageBox.Show("Input Error ");
                return;
            }
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void datePick_ValueChanged(object sender, EventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tablePanel_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

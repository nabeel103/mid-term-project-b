﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class ReportForm : Form
    {
        
        public ReportForm()
        {
            InitializeComponent();
            initialLoadings();
        }

        private void initialLoadings()
        {
            path.Text = Environment.CurrentDirectory;
            attCombo.Items.Add("Student Report");
            attCombo.Items.Add("CLO Data");
            attCombo.Items.Add("Rubric Data");
            attCombo.Items.Add("Rubric Level Data");
            attCombo.Items.Add("AssessmentComponent Data");
            attCombo.Items.Add("List of ClassAttendance"); 
            attCombo.Items.Add("Total Assessments"); 

            attCombo.SelectedItem = attCombo.Items[0];

        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                path.Text = fbd.SelectedPath;
            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            


            try
            {
                if (!(string.IsNullOrWhiteSpace(reportName.Text)))
                {
                    if(attCombo.SelectedItem.ToString() == "Student Report")
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("select RegistrationNumber,FirstName+' '+LastName as Name , Contact,Email from Student", con);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable studentDt = new DataTable();
                        da.Fill(studentDt);

                        MessageBox.Show("Please wait and click OK...!");
                        
                        Report.GeneratePdf(studentDt, path.Text,"Students",reportName.Text);
                        MessageBox.Show("Report has been genereted");
                    }
                    else if (attCombo.SelectedItem.ToString() == "CLO Data")
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("select* from Clo", con);
                        
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable studentDt = new DataTable();
                        da.Fill(studentDt);

                        MessageBox.Show("Please wait and click OK...!");

                        Report.GeneratePdf(studentDt, path.Text, "CLO", reportName.Text);
                        MessageBox.Show("Report has been genereted");
                        
                    }
                    else if (attCombo.SelectedItem.ToString() == "Rubric Data")
                    {
                        var con = Configuration.getInstance().getConnection();
                        
                        SqlCommand cmd = new SqlCommand("select * from Rubric", con);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable studentDt = new DataTable();
                        da.Fill(studentDt);

                        MessageBox.Show("Please wait and click OK...!");

                        Report.GeneratePdf(studentDt, path.Text, "Rubrics", reportName.Text);
                        MessageBox.Show("Report has been genereted");

                    }
                    else if (attCombo.SelectedItem.ToString() == "Rubric Level Data")
                    {
                        var con = Configuration.getInstance().getConnection();

                        SqlCommand cmd = new SqlCommand("select * from RubricLevel", con);
                        
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable studentDt = new DataTable();
                        da.Fill(studentDt);

                        MessageBox.Show("Please wait and click OK...!");

                        Report.GeneratePdf(studentDt, path.Text, "Rubric Level", reportName.Text);
                        MessageBox.Show("Report has been genereted");

                    }
                    else if (attCombo.SelectedItem.ToString() == "AssessmentComponent Data")
                    {
                        var con = Configuration.getInstance().getConnection();

                        
                        SqlCommand cmd = new SqlCommand("select * from AssessmentComponent", con);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable studentDt = new DataTable();
                        da.Fill(studentDt);

                        MessageBox.Show("Please wait and click OK...!");

                        Report.GeneratePdf(studentDt, path.Text, "Assessment Components", reportName.Text);
                        MessageBox.Show("Report has been genereted");
                        
                    }
                    else if (attCombo.SelectedItem.ToString() == "List of ClassAttendance")
                    {
                        var con = Configuration.getInstance().getConnection();


                        SqlCommand cmd = new SqlCommand("select AttendanceDate from ClassAttendance", con);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable studentDt = new DataTable();
                        da.Fill(studentDt);

                        MessageBox.Show("Please wait and click OK...!");

                        Report.GeneratePdf(studentDt, path.Text, "Class Attedance", reportName.Text);
                        MessageBox.Show("Report has been genereted");
                        
                    }
                    else if (attCombo.SelectedItem.ToString() == "Total Assessments")
                    {
                        var con = Configuration.getInstance().getConnection();


                        SqlCommand cmd = new SqlCommand("select* FROM Assessment", con);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable studentDt = new DataTable();
                        da.Fill(studentDt);

                        MessageBox.Show("Please wait and click OK...!");

                        Report.GeneratePdf(studentDt, path.Text, "Assessments", reportName.Text);
                        MessageBox.Show("Report has been genereted");

                    }
                    else
                        MessageBox.Show("Invalid Selection...!");
                }
                else
                {
                    MessageBox.Show("Report Name is empty!");
                }

            }

            catch (Exception)
            {
                MessageBox.Show("Exceptopn in report");
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

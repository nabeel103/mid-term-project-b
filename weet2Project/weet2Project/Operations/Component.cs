﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class Component : Form
    {
        private static AssessmentComponent  assComponent = new AssessmentComponent();
        public Component()
        {
            InitializeComponent();
            initialLoading();
        }
        private void initialLoading()
        {
            rubIdCombo.Items.Clear();


            List<Rubric> rubricList = Rubric.getRubricList();
            foreach (Rubric rb in rubricList)
            {
                rubIdCombo.Items.Add(rb.Id);
            }
            if (rubIdCombo.Items.Count > 0)
            {
                rubIdCombo.SelectedItem = rubIdCombo.Items[0];
            }

            List<Assessment> assList = Assessment.getAssessmentList();
            foreach (Assessment ast in assList)
            {
                assCombo.Items.Add(ast.Title);
            }
            if (assCombo.Items.Count > 0)
            {
                assCombo.SelectedItem = assCombo.Items[0];
            }
            ShowCOmponentTable();
        }

        private void ShowCOmponentTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            compTable.DataSource = dt;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rubIdCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rubIdCombo.Text.Length > 0)
            {
                compDetails.Text = Rubric.getRubric(Int32.Parse(rubIdCombo.Text)).Details;
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (!checkField())
            {
                if (assComponent.Id == -1) 
                    MessageBox.Show("Error...");
                else
                {
                    if (assComponent.RubricId == -1)
                        MessageBox.Show("Warning: Select a valid Rubric!");
                    if (assComponent.AssessmentId == -1) 
                        MessageBox.Show("Warning: Select a valid Assessment!");

                    assComponent.Name = compName.Text;
                    assComponent.RubricId = Int32.Parse(rubIdCombo.Text);
                    assComponent.TotalMarks = Int32.Parse(CompMarks.Text);
                    assComponent.AssessmentId = Assessment.getAssessment(assCombo.Text).Id;

                    int count = AssessmentComponent.addUpdateAssessmentComponent(assComponent);

                    if (count == 0) 
                        MessageBox.Show("Error in Updating...");
                    else if (count == 1)
                        MessageBox.Show("Added Successfully...");
                    else if (count == 2) 
                        MessageBox.Show("Updated Successfully...");

                    emptyFields();
                    assComponent.Id = -1;
                    initialLoading();
                }
               
            }
            else
            {
                MessageBox.Show("Error in Fields");
                return;
            }
            
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (assComponent.Id != -1)
            {
                if (!checkField())
                {
                    if (AssessmentComponent.deleteAssessmentComponent_Id(assComponent.Id)) 
                        MessageBox.Show("Deleted Successfully");
                    else 
                        MessageBox.Show("Delete Failed");
                    
                    emptyFields();
                    assComponent.Id = -1;
                    initialLoading();
                    
                }
                else
                {
                    MessageBox.Show("Check Input Data");
                    return;
                }
                
                
            }
            else
            {
                MessageBox.Show("Select From Table");
                return;
            }
            
        }
        private void emptyFields()
        {
            compName.Text = "";
            CompMarks.Text = "";
            
        }
        private bool checkField()
        {
            if (!(string.IsNullOrWhiteSpace(compName.Text) || string.IsNullOrWhiteSpace(compDetails.Text) || string.IsNullOrWhiteSpace(rubIdCombo.Text) || string.IsNullOrWhiteSpace(assCombo.Text)))
            {
                return false;
            }
            else 
                return true;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (!checkField())
            {
                assComponent.Id = -1;
                assComponent.Name = compName.Text;
                assComponent.TotalMarks = Int32.Parse(CompMarks.Text);
                assComponent.RubricId = Int32.Parse(rubIdCombo.Text);
                assComponent.AssessmentId = Assessment.getAssessment(assCombo.Text).Id;


                if (assComponent.RubricId == -1) 
                    MessageBox.Show("Select a valid CLO!");
                if (assComponent.AssessmentId == -1)
                    MessageBox.Show("Select a valid Assessment!");
                else
                {
                    if (AssessmentComponent.addUpdateAssessmentComponent(assComponent) == 1) 
                        MessageBox.Show("Added Successfully");
                    else 
                        MessageBox.Show("Error in adding");
                }


                assComponent.Id = -1;

                initialLoading();
                emptyFields();

               
            }
            else
            {
                MessageBox.Show("Error in selecting fields");
                return;
            }
            
            
        }

        private void compTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (e.RowIndex >= 0)
                {
                    //Console.WriteLine(e.RowIndex);
                    DataGridViewRow row = this.compTable.Rows[e.RowIndex];

                    //Load In object 
                    AssessmentComponent a = AssessmentComponent.getAssessmentComponent_Id(assComponent.Id);

                    assComponent.Id = Convert.ToInt32(row.Cells["Id"].Value.ToString());
                    assComponent.Name = row.Cells["Name"].Value.ToString();
                    assComponent.RubricId = Convert.ToInt32(row.Cells["RubricId"].Value.ToString());
                    assComponent.TotalMarks = Convert.ToInt32(row.Cells["TotalMarks"].Value.ToString());
                    assComponent.DateCreated = a.DateCreated;
                    assComponent.DateUpdated = a.DateUpdated;
                    assComponent.AssessmentId = Convert.ToInt32(row.Cells["AssessmentId"].Value.ToString());

                    //Load Fields 
                    compName.Text = assComponent.Name;
                    CompMarks.Text = assComponent.TotalMarks.ToString();
                    rubIdCombo.Text = Rubric.getRubric(assComponent.RubricId).Id.ToString();
                    compDetails.Text = Rubric.getRubric(assComponent.RubricId).Details;
                    assCombo.Text = Assessment.getAssessment_Id(assComponent.AssessmentId).Title;

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error in Selecting Data");
            }
        }
    }
}

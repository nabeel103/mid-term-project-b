﻿
namespace weet2Project.Operations
{
    partial class MarksBarControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.evaluationCombo = new System.Windows.Forms.ComboBox();
            this.assComp = new System.Windows.Forms.Label();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2Elipse2 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.RubricLevel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // evaluationCombo
            // 
            this.evaluationCombo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.evaluationCombo.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationCombo.ForeColor = System.Drawing.Color.White;
            this.evaluationCombo.FormattingEnabled = true;
            this.evaluationCombo.Location = new System.Drawing.Point(377, 9);
            this.evaluationCombo.Name = "evaluationCombo";
            this.evaluationCombo.Size = new System.Drawing.Size(160, 25);
            this.evaluationCombo.TabIndex = 23;
            // 
            // assComp
            // 
            this.assComp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.assComp.AutoSize = true;
            this.assComp.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assComp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.assComp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.assComp.Location = new System.Drawing.Point(15, 13);
            this.assComp.Name = "assComp";
            this.assComp.Size = new System.Drawing.Size(34, 17);
            this.assComp.TabIndex = 22;
            this.assComp.Text = "abc";
            this.assComp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.BorderRadius = 30;
            this.guna2Elipse1.TargetControl = this.evaluationCombo;
            // 
            // guna2Elipse2
            // 
            this.guna2Elipse2.BorderRadius = 40;
            this.guna2Elipse2.TargetControl = this;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Location = new System.Drawing.Point(121, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 17);
            this.label1.TabIndex = 24;
            this.label1.Text = "abc";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RubricLevel
            // 
            this.RubricLevel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RubricLevel.AutoSize = true;
            this.RubricLevel.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RubricLevel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(118)))), ((int)(((byte)(212)))));
            this.RubricLevel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RubricLevel.Location = new System.Drawing.Point(240, 13);
            this.RubricLevel.Name = "RubricLevel";
            this.RubricLevel.Size = new System.Drawing.Size(34, 17);
            this.RubricLevel.TabIndex = 25;
            this.RubricLevel.Text = "abc";
            this.RubricLevel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MarksBarControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.Controls.Add(this.RubricLevel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.evaluationCombo);
            this.Controls.Add(this.assComp);
            this.Name = "MarksBarControl";
            this.Size = new System.Drawing.Size(552, 45);
            this.Load += new System.EventHandler(this.MarksBarControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox evaluationCombo;
        private System.Windows.Forms.Label assComp;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse2;
        private System.Windows.Forms.Label RubricLevel;
        private System.Windows.Forms.Label label1;
    }
}

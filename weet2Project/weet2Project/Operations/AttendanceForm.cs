﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class AttendanceForm : Form
    {
        String registrationNumber = "";
        Lookup lookup;
        DateTime attDate;
        public AttendanceForm()
        {
            InitializeComponent();
            InitialLoadings();

        }
        public void LoadMarkAtt(DateTime d, string reg, Lookup l, bool delete)
        {
            MarkAttendancePage m = new MarkAttendancePage(d, reg, l,delete);
            m.Show();
        }
        private void InitialLoadings()
        {
            showAttendaceTable();
            loadClassAtt();
        }
        private void loadClassAtt()
        {
            attCombo.Items.Clear();
            List<ClassAttendance> list = ClassAttendance.getClassAttendance();
            foreach (ClassAttendance ca in list)
            {
                attCombo.Items.Add(ca.AttendanceDate.ToShortDateString());
            }

            if (attCombo.Items.Count > 0 && attCombo.Text == "")
            {
                attCombo.SelectedItem = attCombo.Items[0];
            }
        }
        private void showAttendaceTable()
        {
            attendanceTable.Rows.Clear();

            List<StudentAttendance> studentAttendanceList = StudentAttendance.getStudentAttendanceList();
            foreach (StudentAttendance rslt in studentAttendanceList)
            {
                int n = attendanceTable.Rows.Add();
                attendanceTable.Rows[n].Cells[0].Value = ClassAttendance.getClassAttendance(rslt.AttendanceId).AttendanceDate.ToShortDateString();
                attendanceTable.Rows[n].Cells[1].Value = StudentClass.getStudent(rslt.StudentId).RegistrationNumber.ToString();
                attendanceTable.Rows[n].Cells[2].Value = Lookup.getLookup(rslt.AttendanceStatus).Name;
            }
        }
        private void AttendanceForm_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void attendanceTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                attCombo.Text = attendanceTable.Rows[e.RowIndex].Cells["Date"].Value.ToString();



                int month = Int32.Parse(attCombo.Text.Split('/')[0]);
                int day = Int32.Parse(attCombo.Text.Split('/')[1]);
                int year = Int32.Parse(attCombo.Text.Split('/')[2]);

                DateTime date = new DateTime(year, month, day);
                
                attDate = date;


                StudentClass student = StudentClass.getStudent__RegistrationNumber(attendanceTable.Rows[e.RowIndex].Cells["regNo"].Value.ToString());
                lookup = Lookup.getLookup(attendanceTable.Rows[e.RowIndex].Cells["status"].Value.ToString(), "ATTENDANCE_STATUS");

                registrationNumber = student.RegistrationNumber;
                
            }
        }
        private bool checkFields()
        {
            if (!(string.IsNullOrWhiteSpace(attCombo.Text)))
                return false;
            else
                return true;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (!checkFields())
            {


                int month = Int32.Parse(attCombo.Text.Split('/')[0]);
                int day = Int32.Parse(attCombo.Text.Split('/')[1]);
                int year = Int32.Parse(attCombo.Text.Split('/')[2]);

                DateTime date = new DateTime(year, month, day);

                attDate = date;
                LoadMarkAtt(attDate, registrationNumber, lookup, false);

                attDate = new DateTime();
                registrationNumber = "";
               
            }
            else
            {
                MessageBox.Show("Wrong Input");
                return;
            }
            


        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            showAttendaceTable();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            ClassAttendance att = new ClassAttendance();
            ClassAttendance.addUpdateClassAttendance(att);
            attDate = DateTime.Now;
            attCombo.Text = attDate.ToShortDateString();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (registrationNumber!="")
            {
                LoadMarkAtt(attDate, registrationNumber, lookup, true);
            }
            else
            {
                MessageBox.Show("Please select from Table");

            }
            registrationNumber = "";
            attDate = new DateTime(); 
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
        
}

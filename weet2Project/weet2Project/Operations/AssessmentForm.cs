﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class AssessmentForm : Form
    {
        private Assessment assessment = new Assessment();
        public AssessmentForm()
        {
            InitializeComponent();
            showAssessmentTable();
        }

        private void showAssessmentTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            assTable.DataSource = dt;
        }

        private bool checkFields()
        {
            
            if(!(string.IsNullOrWhiteSpace(assMarks.Text) || string.IsNullOrWhiteSpace(assMarks.Text) || string.IsNullOrWhiteSpace(assWeightage.Text)))
            {
                return false;
            }
            return true;

        }
        private void emptyFields()
        {
            assTitle.Text = "";
            assMarks.Text = "";
            assWeightage.Text = "";

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (!checkFields())
            {
                assessment.Id = -1;
                assessment.Title = assTitle.Text;
                assessment.TotalMarks = Int32.Parse(assMarks.Text);
                assessment.TotalWeightage = Int32.Parse(assWeightage.Text);

                if (Assessment.addORupdateAssessment(assessment) == 1)
                {
                    MessageBox.Show("Added Successfully");
                }
                else MessageBox.Show("Something went wrong");

                emptyFields();
                assessment.Id = -1;
                showAssessmentTable();

               
            }
            else
            {
                MessageBox.Show("Wrong Input");
                return;
            }
            
        }

        private void assTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                if (e.RowIndex >= 0)
                {
                    //Console.WriteLine(e.RowIndex);
                    DataGridViewRow row = this.assTable.Rows[e.RowIndex];

                    assTitle.Text = row.Cells["Title"].Value.ToString();
                    assWeightage.Text = row.Cells["TotalWeightage"].Value.ToString();
                    assMarks.Text = row.Cells["TotalMarks"].Value.ToString();

                    assessment.Id = Convert.ToInt32(row.Cells["Id"].Value.ToString());
                    assessment.Title = assTitle.Text;
                    assessment.TotalMarks = Convert.ToInt32(assMarks.Text);
                    assessment.TotalWeightage = Convert.ToInt32(assWeightage.Text);
                    assessment.DateCreated = (Assessment.getAssessment_Id(assessment.Id)).DateCreated;

                }
            }

            catch (Exception)
            {
                MessageBox.Show("Error in Selecting Data");
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (!checkFields())
            {
                if (assessment.Id == -1)
                {
                    assessment.Title = assTitle.Text;
                    assessment.TotalMarks = Int32.Parse(assMarks.Text);
                    assessment.TotalWeightage = Int32.Parse(assWeightage.Text);

                    int count = Assessment.addORupdateAssessment(assessment);

                    if (count == 1)
                        MessageBox.Show("Added Successfully!");
                    else if (count == 2)
                        MessageBox.Show("Updated Successfully!");
                    else if (count == 0)
                        MessageBox.Show("Something went wrong");

                    assessment.Id = -1;
                    emptyFields();
                    showAssessmentTable();
                    
                }

                else
                {
                    MessageBox.Show("Invalid Data selection");

                }

            }
            else
            {
                MessageBox.Show("Wrong Input");
                return;
            }
            
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (assessment.Id != -1)
            {
                if (!checkFields())
                {
                    if (Assessment.deleteAssessment_Id(assessment.Id)) 
                        MessageBox.Show("Deleted Successfully");
                    else 
                        MessageBox.Show("Error in deleting");

                    showAssessmentTable();
                    emptyFields();
                    assessment.Id = -1;
                    
                }
                else
                {
                    MessageBox.Show("Wrong Input");
                    return;
                }
                
               
            }
            else
            {
                MessageBox.Show("Invalid Data selection");
                return;
            }
           
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tablePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void assMarks_TextChanged(object sender, EventArgs e)
        {

        }

        private void assTitle_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void assWeightage_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Classes;

namespace weet2Project.Operations
{
    public partial class Student : Form
    {
        private StudentClass student = new StudentClass();
        public Student()
        {
            InitializeComponent();
            showStuddentTable();
            LoadsStatusCombobox();

        }

        private void showStuddentTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            studentTable.DataSource = dt;
        }
        private bool checkFields()
        {
            if (!(string.IsNullOrWhiteSpace(fName.Text) || string.IsNullOrWhiteSpace(lastName.Text) || string.IsNullOrWhiteSpace(contact.Text) || string.IsNullOrWhiteSpace(email.Text) || string.IsNullOrWhiteSpace(regNo.Text) || string.IsNullOrWhiteSpace(statusCombo.Text)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private bool emptyFields()
        {
            fName.Text = "";
            lastName.Text = "";
            regNo.Text = "";
            contact.Text = "";
            email.Text = "";
            return true;

        }
        private bool LoadsStatusCombobox()
        {
            List<Lookup> lookupList = Lookup.getLookupList_category("STUDENT_STATUS");
            foreach (Lookup lk in lookupList)
            {
                statusCombo.Items.Add(lk.Name);
            }
            return true;
        }


        private void Student_Load(object sender, EventArgs e)
        {

        }

        private void guna2TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void studentTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void deleteButton_Click(object sender, EventArgs e)
        {

        }

        private void addButton_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void updateButton_Click(object sender, EventArgs e)
        {

        }

        private void tablePanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void depart_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void session_TextChanged(object sender, EventArgs e)
        {

        }

        private void address_TextChanged(object sender, EventArgs e)
        {

        }

        private void studName_TextChanged(object sender, EventArgs e)
        {

        }

        private void regNo_TextChanged(object sender, EventArgs e)
        {

        }

        private void addButton_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void addButton_Click_1(object sender, EventArgs e)
        {
            if (!checkFields())
            {
                student.Id = -1;
                student.FirstName = fName.Text;
                student.LastName = lastName.Text;
                student.Contact = contact.Text;
                student.Email = email.Text;
                student.RegistrationNumber = regNo.Text;
                student.Status = Lookup.getLookup(statusCombo.Text, "STUDENT_STATUS").LookupId;
                if (student.Status == -1)
                {
                    MessageBox.Show("Invalid Status Selection!");
                }
                else
                {
                    if (StudentClass.Add_and_UpdateStudent(student) == 1)
                    {
                        MessageBox.Show("Added Successfully");
                        emptyFields();
                    }
                    else { MessageBox.Show("Adding Failed"); }
                }

                            }
            else
            {
                MessageBox.Show("Invalid Input Attempt!");
                return;

            }
            student.Id = -1;
            showStuddentTable();

        }

        private void studentTable_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex >= 0)
                {
                    //Console.WriteLine(e.RowIndex);
                    DataGridViewRow row = this.studentTable.Rows[e.RowIndex];

                    fName.Text = row.Cells["FirstName"].Value.ToString();
                    lastName.Text = row.Cells["LastName"].Value.ToString();
                    regNo.Text = row.Cells["RegistrationNumber"].Value.ToString();
                    contact.Text = row.Cells["Contact"].Value.ToString();
                    email.Text = row.Cells["Email"].Value.ToString();

                    student.Id = Convert.ToInt32(row.Cells["Id"].Value.ToString());
                    if (row.Cells["Status"].Value.ToString() == "5")
                    {
                        statusCombo.Text = "Active";
                        student.Status = 1;

                    }
                    else
                    {
                        statusCombo.Text = "InActive";
                        student.Status = 0;
                    }
                    student.FirstName = fName.Text;
                    student.LastName = lastName.Text;
                    student.Contact = contact.Text;
                    student.Email = email.Text;
                    student.RegistrationNumber = regNo.Text;





                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error in Selecting Data");
            }
        }

        private void updateButton_Click_1(object sender, EventArgs e)
        {
            //update

            if (!checkFields())
            {

                if (student.Id == -1)
                {
                    MessageBox.Show("Click on valid Row!");
                }
                else
                {
                    if (student.Status == -1)
                        MessageBox.Show("Error...!");

                    student.FirstName = fName.Text;
                    student.LastName = lastName.Text;
                    student.Contact = contact.Text;
                    student.Email = email.Text;
                    student.RegistrationNumber = regNo.Text;

                    if (statusCombo.Text.Equals("Active"))
                    {
                        student.Status = 5;
                    }
                    else student.Status = 6;


                    int status = StudentClass.Add_and_UpdateStudent(student);
                    if (status == 0)
                        MessageBox.Show("Error in UPDATING!");
                    if (status == 1)
                        MessageBox.Show("ADDED Successfully!");
                    if (status == 2)
                        MessageBox.Show("UPDATED Successfully!");
                    showStuddentTable();
                    emptyFields();
                    student.Id = -1;
                }
            }
            else
            {
                MessageBox.Show("Fields are empty...!");
                return;
            }
        }

        private void deleteButton_Click_1(object sender, EventArgs e)
        {
            if (student.Id != -1)
            {
               

                if (!checkFields())
                {
                    if (StudentClass.deleteStudent(student.Id))
                    {
                        MessageBox.Show("------DELETED Successfully-----");
                    }
                    else
                    {
                        MessageBox.Show("-----Error in deleting students----");
                    }
                    showStuddentTable();
                    emptyFields();
                    student.Id = -1;
                    MessageBox.Show("----Please check Input Fields----");
                    return;
                }
               
            }
            else
            {
                MessageBox.Show("Select a valid Row...!");
                return;
            }
        }

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void studName_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void regNo_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void address_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void session_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label5_Click_1(object sender, EventArgs e)
        {

        }

        private void depart_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void panel2_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void tablePanel_Paint_1(object sender, PaintEventArgs e)
        {

        }
    }
}

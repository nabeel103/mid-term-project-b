﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using weet2Project.Operations;

namespace weet2Project
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            LoadForm(new Student());
        }
        public void LoadForm(Object Form)
        {
            if (this.mainPanel.Controls.Count > 0)
            {
                this.mainPanel.Controls.RemoveAt(0);
            }

            Form f = Form as Form;
            f.TopLevel = false;
            f.Dock = DockStyle.Fill;
            this.mainPanel.Controls.Add(f);
            this.mainPanel.Tag = f;
            f.Show();
        }

        private void moveImageBox(object sender)
        {
            Guna2Button b = (Guna2Button)sender;
            //slideImage.Location = new Point(b.Location.X + 121, b.Location.Y - 30);
            //slideImage.SendToBack();

        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            LoadForm(new Student());
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button1_CheckedChanged(object sender, EventArgs e)
        {
            moveImageBox(sender);
        }

        private void guna2Button2_MouseClick(object sender, MouseEventArgs e)
        {
            LoadForm(new CLOform());
        }

        private void guna2Button3_MouseClick(object sender, MouseEventArgs e)
        {
            //
            LoadForm(new RubricsForm());
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button8_Click(object sender, EventArgs e)
        {
            LoadForm(new ResultForm());
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button7_Click(object sender, EventArgs e)
        {
            LoadForm(new ClassAttendanceForm());
        }

        private void guna2Button6_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button4_MouseClick(object sender, MouseEventArgs e)
        {
            LoadForm(new RubricLevelForm());
        }

        private void guna2Button6_MouseClick(object sender, MouseEventArgs e)
        {
            
            LoadForm(new Operations.Component());
        }

        private void guna2Button10_MouseClick(object sender, MouseEventArgs e)
        {
            LoadForm(new AssessmentForm());
        }

        private void guna2Button5_Click(object sender, EventArgs e)
        {
            LoadForm(new AttendanceForm());
        }

        private void guna2Button9_Click(object sender, EventArgs e)
        {
            LoadForm(new ReportForm());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace weet2Project
{
    class Configuration
    {
        public String ConnectionStr = @"Data Source=DESKTOP-PAGTC75;Initial Catalog=ProjectB;Integrated Security=True";
        SqlConnection con;
        private static Configuration _instance;
        public static Configuration getInstance()
        {
            if (_instance == null)
                _instance = new Configuration();
            return _instance;
        }
        private Configuration()
        {
            con = new SqlConnection(ConnectionStr);
            con.Open();
        }
        public SqlConnection getConnection()
        {
            return con;
        }
        
        public int Execute(string query)
        {
            string conString = this.ConnectionStr;
            int result = 0;
            SqlConnection con = new SqlConnection(conString);
            con.Open();
            if (con.State == System.Data.ConnectionState.Open)
            {
                
                SqlCommand cmd = new SqlCommand(query, con);
                result = cmd.ExecuteNonQuery();
            }
            con.Close();
            return result;
        }
    }
}






